function createNode(value) {
    return {
        value,
        next: null
    }
}

function createLinkedList() {
    return {
        head: null,
        tail: null,
        length: 0,

        push(value) {
            const node = createNode(value);

            if (this.head === null) {
                this.head = node;
                this.tail = node;
                this.length++
                return node;
            }

            this.tail.next = node;
            this.tail = node;
            this.length++

            return node;
        },
        pop() {
            if (this.isEmpty()) {
                return null;
            }

            const node = this.tail;

            if (this.head === this.tail) {
                this.head = null;
                this.tail = null;
                this.length--;
                return node;
            }

            let current = this.head;
            let penultimate;
            while (current) {
                if (current.next === this.tail) {
                    penultimate = current;
                    break;
                }

                current = current.next;
            }

            penultimate.next = null;
            this.tail = penultimate;
            this.length--

            return node;
        },
        isEmpty() {
            return this.length === 0;
        },
        print() {
            const values = [];
            let current = this.head;

            while (current) {
                values.push(current.value);
                current = current.next;
            }

            return values.join(' => ');
        }
    }
}

const list = createLinkedList();

list.push(1);
list.push(2);
list.push(3);
list.pop();
list.pop();

console.log(list.print());