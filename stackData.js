function createStack() {
    const stack = [];

    return {
        push(x) {
            stack.push(x);
        },
        pop() {
            if (stack.length === 0) {
                return undefined;
            }
            return stack.pop();
        },
        peek() {
            if (stack.length === 0) {
                return undefined;
            }
            return stack[stack.length -1];
        },
        get length() {
            return stack.length;
        },
        get stack() {
            return stack;
        },
        isEmpty() {
            return stack.length === 0;
        }
    };
}

const lowerBodyStack = createStack();

lowerBodyStack.push('underwear');
lowerBodyStack.push('socks');
lowerBodyStack.push('pants');
lowerBodyStack.push('shoes');

lowerBodyStack.pop();

console.log(lowerBodyStack.peek());
console.log(lowerBodyStack.stack);

exports.createStack = createStack;

