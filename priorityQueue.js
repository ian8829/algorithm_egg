// Queue example: Priority Queue

const { createQueue } = require('./queueData');

function createPriorityQueue() {
    const lowPriorityQueue = createQueue();
    const highPriorityQueue = createQueue();

    return {
        enqueue(item, isHighPriority = false) {
            isHighPriority
                ? highPriorityQueue.enqueue(item)
                : lowPriorityQueue.enqueue(item)
        },
        dequeue() {
            if (!highPriorityQueue.isEmpty()) {
                return highPriorityQueue.dequeue();
            }

            return lowPriorityQueue.dequeue();
        },
        getQueue() {
            if (!highPriorityQueue.isEmpty()) {
                return highPriorityQueue.getQueue();
            }

            return lowPriorityQueue.getQueue();
        },
        peek() {
            if (!highPriorityQueue.isEmpty()) {
                return highPriorityQueue.peek();
            }

            return lowPriorityQueue.peek();
        },
        get length() {
            return highPriorityQueue.length + lowPriorityQueue.length
        },
        isEmpty() {
            return highPriorityQueue.isEmpty() && lowPriorityQueue.isEmpty();
        }
    };
}

const q = createPriorityQueue();

q.enqueue('A fix here', true);
q.enqueue('A fix here22', true);
q.enqueue('A fix here33');

console.log(q.dequeue());
console.log(q.peek());
console.log(q.getQueue());

exports.createPriorityQueue = createPriorityQueue;
