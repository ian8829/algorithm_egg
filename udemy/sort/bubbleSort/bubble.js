function numCompare(num1, num2) {
    return num1 - num2;
}

function lenCompare(str1, str2) {
    return str1.length - str2.length;
}


const example = [ 6, 4, 15, 10 ].sort(numCompare);
const example2 = ["Stelle", "Colt", "Data Structure", "Algorithms"]
    .sort(lenCompare);
    
console.log(example);
console.log(example2);