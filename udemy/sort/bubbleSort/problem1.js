// Start looping from with a var called i 
// the end of the array towards the beginning
// Start an inner loop with a variable
// called j from the beginning until i-1
// if arr[j] is greater than arr[j + 1], 
// swap those two values!
// return the sorted array

const swap = (arr, idx1, idx2) => {
    [arr[idx1], arr[idx2]] = [arr[idx2], arr[idx1]];
};

function bubbleSort(arr) {
    let noSwaps;
    for (let i = arr.length; i > 0; i--) {
        noSwaps = true;
        for (let j = 0; j < arr.length; j++) {
            if (arr[j] > arr[j + 1]) {
                swap(arr, j, j+1);
                noSwaps = false;
            }
        }
        if (noSwaps) break;
    }
    return arr;
}

const result = bubbleSort([1, 7, 2, 20, 5]);

console.log(result);