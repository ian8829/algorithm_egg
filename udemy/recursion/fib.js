// Write a recursive function called fib 
// which accepts a number and returns the nth number
// in the Fibonacci sequence.
// Recall that the fb seq is the seq 
// of whole numbers 1, 1, 2, 3, 5, 8...

// fib(4) // 3
// fib(10) // 55

function fib(n) {
    if (n <= 2) return 1;
    return fib(n-1) + fib(n-2);
}


